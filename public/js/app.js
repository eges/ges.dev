;(function () {
    window.getVH = () => window.innerHeight || document.documentElement.clientHeight || 1;
    window.getVW = () => window.innerWidth || document.documentElement.clientWidth || 1;

    window.isTouch = window.matchMedia?.("(pointer: coarse)").matches || false;

    localStorage.__proto__.pop = function (item) {
        const value = localStorage.getItem(item);
        if (value !== null) localStorage.removeItem(item);
        return value;
    };

    Object.defineProperty(Element.prototype, "inViewport", {
        get: function () {
            const rect = this.getBoundingClientRect();
            return (
                rect.x <= getVW() && rect.right >= 0 &&
                rect.y <= getVH() && rect.bottom >= 0
            );
        }
    });

    Object.defineProperty(HTMLElement.prototype, "offsetDocumentTop", {
        get: function () {
            let element = this,
                offset = this.offsetTop;
            while ((element = element.parentElement))
                offset += element.offsetTop;
            return offset;
        }
    });
})();

;(function () {
    /** Transformations **/
    function emify(elm, rec) {
        const href = emify.schemaPrefix() + rec + emify.at() + emify.mDomain();
        elm.setAttribute("href", href);
    }
    emify.schemaPrefix = () => ([109, 97, 105, 108, 116, 111].map((v) => String.fromCharCode(v)).join("")) + ":";
    emify.at = () => String.fromCharCode(64);
    emify.mDomain = () => document.querySelector("meta[name=domain]").getAttribute("content");

    function tlify(elm, n) {
        elm.setAttribute("href", tlify.schemaPrefix() + n);
    }
    tlify.schemaPrefix = () => ([116, 101, 108, 58, 43].map((v) => String.fromCharCode(v)).join(""));

    /**
     * @param {string|NodeListOf<Element>} query
     * @param {function(Element)} callback
     */
    function bindInteraction(query, callback) {
        const elms = (typeof query === "string") ? document.querySelectorAll(query) : query,
            evh = () => elms.forEach(elm => callback(elm));
        document.addEventListener("mousedown", evh);
        document.addEventListener("selectionchange", evh);
        elms.forEach(elm => elm.addEventListener("mouseover", () => callback(elm)));
    }

    document.addEventListener("DOMContentLoaded", () => {

        document.querySelectorAll("[data-discombobulate]").forEach(elm => {
            const disco = elm.getAttribute("data-discombobulate");
            if (disco) elm.innerHTML = atob(disco);
            elm.removeAttribute("data-discombobulate");
            elm.setAttribute("data-recombobulated", "");
        });

        bindInteraction("a[data-mt]", elm => {
            const rec = elm.getAttribute("data-mt");
            if (rec) emify(elm, rec);
            elm.removeAttribute("data-mt");
        });

        bindInteraction("a[data-tt]", elm => {
            const nrv = elm.getAttribute("data-tt");
            if (nrv) tlify(elm, nrv.split("").reverse().join(""));
            elm.removeAttribute("data-tt");
        });

        bindInteraction(".mirror", elm => {
            if (!elm.classList.contains("mirror")) return;
            elm.innerText = elm.innerText.split("").reverse().join("");
            elm.classList.remove("mirror");
        });

        bindInteraction(".mat", elm => {
            if (!elm.classList.contains("mat")) return;
            elm.innerHTML = emify.at();
            elm.classList.remove("mat");
        });

    });

    /** Breadcrumbs **/
    window.setCrumb = str => {
        const crumbs = document.querySelectorAll(".crumb");
        crumbs.forEach(elm => elm.innerText = "~" + (str === false ? "" : "/" + str));
    };

    /** Scroll intro parallax **/
    document.addEventListener("DOMContentLoaded", () => {
        const
            elmSec = document.querySelector('.intro-section'),
            elmImg = document.querySelector('.intro-img'),
            elmCnt = document.querySelector('.intro-container');

        function introParallax() {
            window.requestAnimationFrame(() => {
                const scroll = window.scrollY;
                if (window.innerWidth < 890 || scroll > elmSec.clientHeight) return;
                const translate = Math.round(scroll * 0.1);
                if (elmImg) elmImg.style.transform = "translateY(-" + translate + "px)";
                if (elmCnt) elmCnt.style.transform = "translateY(" + (translate * .4) + "px)";
            });
        }

        if (elmSec) {
            introParallax();
            window.addEventListener("scroll", introParallax);
            window.addEventListener("resize", introParallax);
        }
    });

    /** Toggle visibility / Clients info disclaimer **/
    document.addEventListener("DOMContentLoaded", () => {
       function getAreas(toggles) {
           return document.querySelectorAll(`[data-toggled="${toggles}"]`)
       }
       function onClick(event) {
           getAreas(event.target.dataset.toggles).forEach(elm => elm.toggleAttribute("hidden"));
       }
       document.querySelectorAll("[data-toggles]").forEach(toggler => {
           getAreas(toggler.dataset.toggles).forEach(elm => {
               elm.setAttribute("hidden", "hidden")
           });
           toggler.addEventListener("click", onClick);
       });
    });

    /** Lazyload client background images **/
    document.addEventListener("DOMContentLoaded", () => {
        const elms = document.querySelectorAll("[data-lazy-client]");
        elms.forEach(elm => {
            const client = elm.getAttribute("data-lazy-client");
            elm.setAttribute("data-client", client);
            elm.removeAttribute("data-lazy-client");
        });
    });

    /** Lazyload image sources **/
    document.addEventListener("DOMContentLoaded", () => {
        const elms = document.querySelectorAll("img[data-lazy-src]");
        elms.forEach(elm => {
            const src = elm.getAttribute("data-lazy-src");
            if (src) elm.setAttribute("src", src);
            elm.removeAttribute("data-lazy-src");
        });
    });

    /** Remove script-only attributes **/
    document.addEventListener("DOMContentLoaded", () => {
        const elms = document.querySelectorAll("[data-script-only]");
        elms.forEach(elm => elm.removeAttribute("data-script-only"));
    });

})();

;(function () {
    /** Language **/
    function getElements() {
        return document.querySelectorAll("main > section");
    }

    /** @return {[number, number]} */
    function getTopElement() {
        const elms = getElements();
        let prevDist = null, prevTop = null, i = -1;

        while (++i < elms.length) {
            const elm = elms[i],
                rect = elm.getBoundingClientRect(),
                dist = Math.abs(rect.top);

            if (i > 0 && dist > prevDist)
                return [i - 1, prevTop];

            prevDist = dist;
            prevTop = rect.top;
        }

        return [i - 1, prevTop];
    }

    function prepLangChange() {
        const [i, top] = getTopElement();
        localStorage.setItem("langchange", "1");
        localStorage.setItem("langchange_element", "" + i);
        localStorage.setItem("langchange_offset", "" + top);
    }

    function gotoLangChange() {
        const i = localStorage.getItem("langchange_element") | 0,
            offset = +localStorage.getItem("langchange_offset"),
            elm = getElements()[i];
        document.documentElement.scrollTop = elm.offsetDocumentTop - offset;
    }

    window.prepLangChange = prepLangChange;
    window.gotoLangChange = gotoLangChange;

    const langChange = localStorage.pop("langchange");
    if (langChange) gotoLangChange();

    document.addEventListener("DOMContentLoaded", () => {
        const elm = document.getElementById("lang"),
            cl= elm.classList,
            lang = document.documentElement.getAttribute("lang"),
            indicate = () => cl.add("indicator-" + {"de": "right", "en": "left"}[lang]),
            animate = () => cl.add("animate");
        if (langChange) {
            setTimeout(() => window.requestAnimationFrame(gotoLangChange), 1);
            setTimeout(indicate, 123);
            animate();
        } else {
            setTimeout(animate, 200);
            indicate();
        }
        elm.addEventListener("click", window.prepLangChange);
    });

    document.addEventListener("DOMContentLoaded", () =>
        document.querySelectorAll(".theme-toggler")
            .forEach(elm => elm.addEventListener("click", () => theme.next()))
    );

})();

;(function() {
    /** Showcase 3D Cards **/
    function getFollowerElms() {
        return [
            document.querySelectorAll(".showcase__card"),
            document.querySelectorAll(".showcase__logo__part:first-child"),
            document.querySelectorAll(".showcase__logo__part:last-child")
        ];
    }

    function getFactor(elm, key) {
        const data = "data-" + key;
        return elm[data] || (elm[data] = (Number(elm.getAttribute(data)) || 0));
    }

    function bindFollowCursor() {
        const [cards, logoFirst, logoLast] = getFollowerElms();
        function calcRotation(elm, mouseX, mouseY, fX = 0, fY = 0, fZ = 0) {
            const vw = getVW(), vh = getVH(),
                tiltX = getFactor(elm, "tilt-x"),
                tiltY = getFactor(elm, "tilt-y"),
                tiltZ = getFactor(elm, "tilt-z"),
                mouseXrel = mouseX / vw,
                mouseYrel = mouseY / vh,
                rect = elm.getBoundingClientRect(),
                elmX = rect.x + (rect.right - rect.left) / 2,
                elmY = rect.y + (rect.bottom - rect.top) / 2,
                elmXrel = elmX / vw,
                elmYrel = elmY / vh,
                offXrel = mouseXrel - elmXrel,
                offYrel = mouseYrel - elmYrel,
                offZrel = offXrel * offYrel,
                /*dist = Math.sqrt(offXrel * offXrel + offYrel * offYrel),*/
                x = tiltX * (offYrel) + fX * tiltX,
                y = tiltY * (offXrel) + fY * tiltY,
                z = tiltZ * (offZrel) + fZ * tiltZ;
            return [x.toFixed(2), y.toFixed(2), z.toFixed(2)];
        }
        const calcBrightness = (mouseY) => (-100 * mouseY / getVH() + 1050) * .1;
        function transformCard(elm, mouseX, mouseY) {
            const [x, y, z] = calcRotation(elm, mouseX, mouseY, .3, .1, .25);
            const brightness = calcBrightness(mouseY);
            elm.style.filter = `brightness(${brightness}%)`;
            elm.style.transform = `scale3d(.9, .9, .9) rotateX(${x}deg) rotateY(${y}deg) rotateZ(${z}deg) translate3d(0, 0, -25px)`;
        }
        function transformLogo(elm, mouseX, mouseY, tZ) {
            const [x, y, _] = calcRotation(elm, mouseX, mouseY);
            const brightness = calcBrightness(mouseY);
            elm.style.filter = `brightness(${brightness}%)`;
            elm.style.transform = `scale3d(1, 1, 1) rotateX(${x}deg) rotateY(${y}deg) rotateZ(10deg) translate3d(0, 0, ${tZ}px)`;
        }
        function addEventForElm(callback, ...args) {
            return function (elm) {
                document.addEventListener("mousemove", (event) => {
                    window.requestAnimationFrame(() => {
                        const mouseX = event.clientX, mouseY = event.clientY;
                        if (elm.inViewport)
                            callback(elm, mouseX, mouseY, ...args);
                    });
                });
            }
        }
        cards.forEach(addEventForElm(transformCard));
        logoFirst.forEach(addEventForElm(transformLogo, 10));
        logoLast.forEach(addEventForElm(transformLogo, 25));
    }

    function bindFollowScroll() {
        const [cards, _, __] = getFollowerElms();
        function transformCard(elm) {
            const vh = getVH(), hvh = vh / 2,
                rect = elm.getBoundingClientRect(),
                elmY = rect.y + (rect.bottom - rect.top) / 2,
                elmYrel = (elmY / hvh) - 1,
                x = 15 * elmYrel - 3,
                y = -10 * elmYrel + 15,
                z = .18 * Math.pow(elmYrel, 2) - 1.5;
            elm.style.transform = `scale3d(.9, .9, .9) rotateX(${x.toFixed(2)}deg) rotateY(${y.toFixed(2)}deg) rotateZ(${z.toFixed(2)}deg) translate3d(0, 0, -25px)`;
        }
        function parallax(elm) {
            window.requestAnimationFrame(() => {
                if (elm.inViewport) transformCard(elm);
            });
        }
        cards.forEach(elm => {
            parallax(elm);
            document.addEventListener("scroll", () => parallax(elm));
            document.addEventListener("zoom", () => parallax(elm));
        });
    }

    document.addEventListener("DOMContentLoaded", isTouch ? bindFollowScroll : bindFollowCursor);

})();
