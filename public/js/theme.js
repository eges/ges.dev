/**
 * Thematic.
 * A neat little theming library.
 * @author Eric Gesemann <eric.ges.dev@gmail.com>
 */

"use strict";

/**
 * @typedef {Object.<string, string>} ThemeVariants
 */

/**
 * @typedef {Object} ThemeOptions
 * @property {boolean} [autoload=true] Specifies whether the theme library should be initialized on page load.
 * @property {ThemeVariants} [variants={}] An object containing theme variant options.
 * @property {boolean} [useBaseVariants=true] Specifies whether to include base "light" and "dark" variants.
 * @property {string} [defaultLight] The default light theme variant. Defaults to `"light"`.
 * @property {string} [defaultDark] The default dark theme variant. Defaults to `"dark"`.
 * @property {string|null} [globalName="theme"] If set to a non-empty string, will be used to reference the theme library on `window.<globalName>`.
 * @property {string|null} [storageKey="theme"] If set to a non-empty string, will be the key to store the current theme variant in `localStorage`, and disables `localStorage` otherwise.
 * @property {boolean} [HTMLDataAttribute=true] Specifies whether to use HTML data attributes to set theme variants.
 *   For example: `<html data-theme="{variant}">`. Defaults to `true`.
 * @property {boolean} [HTMLVariantAttribute=false] If set to true, the active schema's name will be added as an attribute to the HTML tag.
 *   For example: `<html {variant}="true">`. Defaults to `false`.
 *   *Important: This feature violates HTML standards when enabled. Use at your own risk.*
 */

(function () {

    /**
     * Executes a callback function when the document has finished loading. If the document is already loaded, the callback will be executed immediately.
     * @param {Function} callback The function to be executed when the document is loaded.
     * @param {...*} args Optional arguments to be passed to the callback function.
     */
    function whenDOMLoaded(callback, ...args) {
        if (document.readyState === "loading") {
            document.addEventListener("DOMContentLoaded", () => callback(...args));
        } else {
            callback(...args);
        }
    }

    class ThemeError extends Error {
    }

    class DOMManipulator {
        /**
         * Sets the HTML variant attribute based on the provided variant name.
         * This method removes all existing variant attribute values from the given element and adds the provided schema as a new attribute.
         * @param {Thematic} theme The {@link Thematic} instance.
         * @param {string|false} variant The schema to be set as the variant attribute.
         * @param {HTMLElement} element The HTML element on which the variant attribute should be set.
         * @param {string} [prefix=""] A prefix to the attribute name.
         * @experimental
         */
        static setVariantAttribute(theme, variant, element, prefix = "") {
            theme.forEach(t => t && typeof t === "string" && element.removeAttribute(prefix + t));
            if (variant) element.setAttribute(prefix + String(variant), "true");
        }

        /**
         * Updates the attribute of all elements matching the given attribute selector.
         * @param {string} attr The attribute name to update.
         * @param {string|false} value The new value for the attribute.
         * @throws {ThemeError} If either `attr` or `value` is not a string.
         */
        static updateAttribute(attr, value) {
            if (typeof attr !== "string" && (typeof value !== "string" || typeof value !== false)) {
                throw new ThemeError("DOMManipulator.updateAttribute(attr, value) expects two string parameters.")
            }
            document.querySelectorAll(`[${attr}]`)
                .forEach(elm => elm.setAttribute(attr, value || ""));
        }
    }


    /** Represents a result of found themes. */
    class ThemeFinderResult {
        /**
         * @param {ThemeOptions} options The found options for the theme.
         * @param {*} [source=undefined] The source of the found options.
         */
        constructor(options, source = undefined) {
            this.options = options;
            this.source = source;
        }
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Class representing a theme manager that handles theme switching and management.
     */
    class Thematic {

        /** @type {boolean} */
        static created = false;

        /** @type {ThemeOptions} */
        static defaultOptions = {
            autoload: true,
            variants: {},
            useBaseVariants: true,
            globalName: "theme",
            storageKey: "theme",
            defaultLight: undefined,
            defaultDark: undefined,
            HTMLDataAttribute: true,
            HTMLVariantAttribute: false
        };

        /** @type {ThemeVariants} */
        static defaultVariants = {LIGHT: "light", DARK: "dark"};

        /**
         * If no {@link Thematic} instance has been created previously, this method tries to find theme options with
         * {@link Thematic#findThemeOptions}. If the found options do not set `autoload: false` or if no options where
         * found, creates a new instance and initializes it (see {@link Thematic#load}).
         * @returns {Thematic|null}
         */
        static autolaod() {
            if (this.created) return null;

            const optionsFound = Thematic.findThemeOptions(),
                autoload = optionsFound.options?.autoload,
                isset = typeof autoload !== "undefined";

            if ((isset && autoload) || (!isset && Thematic.defaultOptions.autoload)) {
                return new Thematic(optionsFound).kickstart();
            }

            return null;
        }

        /**
         * Retrieves theme options based on the available sources.
         * The available sources are:
         * - window.FARBSCHEMA (if defined)
         * - script tag with type="application/json" and id="farbschema"
         * @returns {ThemeFinderResult} The theme options object.
         */
        static findThemeOptions() {
            window.FARBSCHEMA = window.FARBSCHEMA || undefined;
            if (typeof window.FARBSCHEMA !== "undefined") {
                return new ThemeFinderResult(window.FARBSCHEMA, "var");
            }

            let elm = document.querySelector("script[type=\"application/json\"]#farbschema");
            if (elm) {
                return new ThemeFinderResult(JSON.parse(elm.textContent), elm);
            }

            return new ThemeFinderResult({}, undefined);
        }

        /**
         * Extends a {@link ThemeOptions} object or the given options of a {@link ThemeFinderResult} with the default properties.
         * @param {ThemeOptions|ThemeFinderResult|Object} arg The options to extend or an object containing `source` and `options`.
         * @param {ThemeOptions} arg.options The options to extend.
         * @param {*} [arg.source] A source describing where the options have been gathered. Mainly for debugging purposes.
         * @returns {ThemeOptions} The extended options object.
         */
        static extendOptions(arg) {
            const
                source = arg.hasOwnProperty("source") ? arg.source : undefined,
                proto = Object.defineProperty({}, "source", {value: source});

            let options = arg.hasOwnProperty("options") ? arg.options : arg;
            options = Object.assign({__proto__: proto}, this.defaultOptions, options);

            options.variants = Object.assign({}, options.useBaseVariants ? this.defaultVariants : {}, options.variants);
            const keysOfVariants = Object.keys(options.variants);
            options.defaultLight = options.defaultLight || options.variants?.LIGHT || keysOfVariants[0];
            options.defaultDark = options.defaultDark || options.variants?.DARK || keysOfVariants[(keysOfVariants.length > 1) | 0];

            return options;
        }

        /**
         * @private
         * @type {Object.<string, Array.<Function>>}
         */
        #events;

        /**
         * @private
         * @type {ThemeOptions}
         */
        #options;

        /**
         * Create a theme manager
         * @param {ThemeOptions|ThemeFinderResult} options
         * */
        constructor(options) {
            this.#events = {};
            this.#options = Thematic.extendOptions(options);
            this.created = true;
        }

        /**
         * Retrieves the type of `ThemeError`.
         * @returns {typeof ThemeError} The type of the `ThemeError` class.
         */
        get Error() {
            return ThemeError;
        }

        /** @returns {ThemeOptions} The options for the theme. */
        get options() {
            return this.#options;
        }

        /** @returns {ThemeVariants} The theme variants of the object. */
        get variants() {
            return this.options.variants;
        }

        /** @return {string[]} An array containing all the variant values. */
        get variantValues() {
            return Object.values(this.variants);
        }

        /** @returns {string} The currently active theme variant. */
        get current() {
            return this.contains(this.stored) ? this.stored : this.preferred;
        }

        /** @returns {string} The preferred theme variant according to the user's set color scheme. */
        get preferred() {
            return (
                window.matchMedia?.("(prefers-color-scheme: dark)").matches
                    ? this.options.defaultDark
                    : this.options.defaultLight
            ) || this.options.defaultLight;
        }

        /** @returns {string} The previous value in the sequence of variants. */
        get seqPrev() {
            const vals = this.variantValues, i = vals.indexOf(this.current);
            return (i > -1) ? vals[(vals.length + i - 1) % vals.length] : vals[0];
        }

        /** @returns {string} The next value in the sequence of variants. */
        get seqNext() {
            const vals = this.variantValues, i = vals.indexOf(this.current);
            return (i > -1) ? vals[(i + 1) % vals.length] : vals[0];
        }

        /** @returns {string|false} The next value in the sequence. If there are no more variants, returns false. */
        get nextUp() {
            if (this.stored && this.current === this.preferred) return false;
            return this.seqNext;
        }

        /** @returns {string|false} The theme variant stored in `localStorage`, false if none is set. */
        get stored() {
            return localStorage.getItem(this.options.storageKey) || false;
        }

        /** @param {string|false} variant The theme variant to be stored in `localStorage`. */
        set stored(variant) {
            const key = this.options.storageKey;
            if (!key) return;
            if (variant === false) localStorage.removeItem(key);
            else localStorage.setItem(key, variant);
        }

        /**
         * Assigns an event subscriber function to an event.
         * @param {string} event The name of the event to attach the listener to.
         * @param {Function} callback The event handler function to be executed when the event is triggered.
         */
        on(event, callback) {
            if (typeof this.#events[event] !== 'object') {
                this.#events[event] = [];
            }
            this.#events[event].push(callback);
        }

        /**
         * Emits the specified event and passes the given arguments to all registered listeners.
         * @param {string} event The name of the event to emit.
         * @param {...*} args The arguments to pass to the callbacks.
         */
        emit(event, ...args) {
            if (typeof this.#events[event] === 'object') {
                this.#events[event].forEach(callback => callback.apply(this, args));
            }
        }

        /**
         * Dispatches a "change" event to notify that a change has occurred.
         * @returns {this}
         */
        changed() {
            whenDOMLoaded(() => this.emit("change"));
            return this;
        }

        /**
         * Checks if the given schema is included in the available variants.
         * @param {string} key The schmea to be looked up.
         * @returns {boolean} Returns `true` if the key is an available variant, `false` otherwise.
         */
        contains(key) {
            return key && this.variantValues.indexOf(key) > -1;
        }

        /**
         * Calls the provided callback function for each variant in the collection.
         * @param {function} callback The callback function to be executed for each variant. The callback
         *   function should accept three arguments: currentValue, index, and the entire array of variants.
         */
        forEach(callback) {
            this.variantValues.forEach(callback, this);
        }

        /**
         * Sets the currently active theme variant, if the provided name is available.
         * @param {string|false} variant The color scheme variant name.
         * @returns {string|null} The newly set variant or null if the provided variant is invalid.
         */
        set(variant) {
            if (variant === false) {
                this.stored = false;
            } else if (this.contains(variant)) {
                this.stored = variant;
            } else return null;

            this.changed();

            return variant;
        }

        /**
         * Cycle through the variants in a sequence.
         * @returns {string} The now set variant.
         */
        cycle() {
            return this.set(this.seqNext);
        }

        /**
         * Cycle through the variants in a sequence and reset to system defaults after the last one.
         * @return {string} The now set variant.
         */
        next() {
            return this.set(this.nextUp);
        }

        /**
         * Loads a previously set theme variant or the default.
         * @returns {this} The Thematic instance.
         */
        load() {
            this.set(this.contains(this.stored) ? this.stored : false);
            return this;
        }

        /**
         * Kickstarts the process by binding defaults and loading.
         * @returns {this} The Thematic instance.
         */
        kickstart() {
            return this.bindDefaults().load();
        }

        /**
         * Binds default event listeners to the Thematic instance and a global name if desired.
         * @returns {Thematic} The Thematic instance with the default values bound.
         */
        bindDefaults() {
            this.bindGlobalName();

            if (this.options.HTMLDataAttribute) {
                this.bindDataAttributes();
            }

            if (this.options.HTMLVariantAttribute) {
                this.bindVariantAttribute();
            }

            return this;
        }

        /**
         * Binds this Thematic instance as a property with a given name to `window`.
         * @param {string|null} globalName The property name to assign to `window`. If null, the default will be used.
         *   Providing an empty string or false will prevent binding.
         * @returns {Thematic} The Thematic instance.
         */
        bindGlobalName(globalName = null) {
            if (globalName === "" || globalName === false) {
                return this;
            }
            globalName = globalName || this.options.globalName;
            if (typeof globalName === "string" && globalName) {
                Object.defineProperty(window, globalName, {value: this});
            }
            if (!this) window.theme = theme; // only for IDE support
            return this;
        }

        /**
         * Binds event listeners to change elements' theme attributes values. Targets `"data-theme-{property}"` by default.
         * @param {Object} [properties={}] Property name bindings.
         * @param {string} [properties.current=null] The property referencing the {@link Thematic#current} attribute.
         * @param {string} [properties.nextUp="next"] The property referencing the {@link Thematic#nextUp} attribute.
         * @param {string} [properties.seqNext="seq-next"] The property referencing the {@link Thematic#seqNext} attribute.
         * @param {string} [properties.seqPrev="seq-prev"] The property referencing the {@link Thematic#seqPrev} attribute.
         * @param {string} [prefix="data-theme"] Prefix for the attribute names.
         * @returns {Thematic} This {@link Thematic} instance.
         */
        bindDataAttributes(properties = {}, prefix = "data-theme") {
            properties = Object.assign({
                current: null,
                nextUp: "next",
                seqNext: "seq-next",
                seqPrev: "seq-prev"
            }, properties);

            const attrib = (option) => [prefix, properties[option]].filter(o => o).join('-');

            this.on("change", () => {
                DOMManipulator.updateAttribute(attrib("current"), this.current);
                DOMManipulator.updateAttribute(attrib("nextUp"), this.nextUp);
                DOMManipulator.updateAttribute(attrib("seqNext"), this.seqNext);
                DOMManipulator.updateAttribute(attrib("seqPrev"), this.seqPrev);
            });

            return this;
        }

        /**
         * Binds an event listener that changes a specified element's attributes to reflect the current theme.
         *   Example: `<html data-dark="true">`
         * @param {Element} [element=document.documentElement] The element whose attributes to change.
         * @param {string} [prefix="data-"] The prefix to use for the attribute. Can be an empty string if you desire to violate HTML specs.
         * @returns {Thematic} This Thematic instance.
         */
        bindVariantAttribute(element = document.documentElement, prefix = "data-") {
            this.on("change", () => DOMManipulator.setVariantAttribute(this, this.current, element, prefix));
            return this;
        }
    }

    whenDOMLoaded(() => Thematic.autolaod());

})();
