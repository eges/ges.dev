import base64
import hashlib
import shutil
import sys
import tomllib
from string import Template

import jinja2
import json
import os
import time
import urllib.parse as urlparse

from datetime import datetime, timedelta, timezone
from pathlib import Path

from . import Color


RELEASE_HISTORY_FILENAME = '.releases'


def build_hash(something):
    if something is bytes:
        hashed = hashlib.md5(something)
    else:
        hashed = hashlib.md5(str(something).encode('ascii'))
    return hashed.hexdigest()[:8]


def add_release_version(releases_root: Path, version: str):
    with open(releases_root / RELEASE_HISTORY_FILENAME, 'a') as f:
        f.write(str(version))
        f.write(os.linesep)


def get_last_release_version(releases_root: Path) -> str:
    with open(releases_root / RELEASE_HISTORY_FILENAME, 'rb') as f:
        try:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b'\n':
                f.seek(-2, os.SEEK_CUR)
        except OSError:
            f.seek(0)
        return f.readline().decode().strip()


def recursive_substitute(dictionary: dict, value: str | dict) -> str | dict:
    if type(value) is dict:
        inner = {}
        for inner_key, inner_value in value.items():
            inner[inner_key] = recursive_substitute(dictionary, inner_value)
        return inner
    return Template(value).safe_substitute(dictionary)


class Mixer:

    @staticmethod
    def mix_version(file_path: str, version: str):
        try:
            url_parts = list(urlparse.urlparse(file_path))
            query = dict(urlparse.parse_qsl(url_parts[4]))
            query.update({'e': version})
            url_parts[4] = urlparse.urlencode(query)
            return urlparse.urlunparse(url_parts)
        except:
            return file_path

    @classmethod
    def mix_time(cls, file_path: str):
        v = build_hash(datetime.now())
        return cls.mix_version(file_path, v)

    def __init__(self, assets_path: Path):
        self.assets_path = assets_path
        self.incredients = []

    def jinja_filter(self, asset_uri: str):
        try:
            path = self.assets_path / asset_uri
            try:
                with open(path, 'rb') as f:
                    v = build_hash(f.read())
            except:
                print(Color.FAIL + 'Error: Could not mix file at', path, Color.RESET)
                v = build_hash(os.path.getmtime(path))
            self.incredients.append(asset_uri)
        except:
            return self.mix_time(asset_uri)

        return self.mix_version(asset_uri, v)

    def prepare(self, file_path: Path) -> str | None:
        match file_path.suffix:
            case '.prep':
                return 'preprocess'
        return None

    def mix(self, build_path: Path):
        pass
        # for incredient in self.incredients:
        #     # source = self.assets_path / incredient
        #     # prep = self.prepare(source)
        #     destination = build_path / incredient
        #     destination.parent.mkdir(parents=True, exist_ok=True)
        #     shutil.copy2(self.assets_path / incredient, destination)


class Builder:

    @staticmethod
    def read_langs(langs_path: Path) -> dict[str, dict[str, str]]:
        langs_path = Path(langs_path)
        langs = {}

        for path in langs_path.glob('*.json'):
            with open(path, 'r', encoding='utf-8') as f:
                langs[path.stem] = json.load(f)
            langs[path.stem]['lang'] = path.stem

        for path in langs_path.glob('*.toml'):
            with open(path, 'rb') as f:
                langs[path.stem] = tomllib.load(f)
            langs[path.stem]['lang'] = path.stem
            # print(json.dumps(langs[path.stem], sort_keys=True, indent=4))

        return langs

    @staticmethod
    def get_page_vars(page: str) -> dict[str, str]:
        now = datetime.now(timezone(timedelta(hours=2)))
        compyear = now.strftime('%Y')
        comptime = now.strftime('%Y-%m-%dT%H:%I:%S%z')
        comptime = comptime[:-2] + ':' + comptime[-2:]
        return {
            'page': '' if page == 'index' else page,
            'anorel': 'external nofollow noopener noreferrer noindex',
            'build': build_hash(datetime.now()),
            'compileyear': compyear,
            'compiletime': comptime,
            'prompt': '_'
        }

    def create_jinja_env(self, loader: jinja2.BaseLoader) -> jinja2:
        env = jinja2.Environment(loader=loader)
        env.filters['mix'] = self.mixer.jinja_filter
        env.filters['mix_time'] = Mixer.mix_time
        env.filters['b64encode'] = lambda s: base64.urlsafe_b64encode(str(s).encode()).decode()
        env.filters['b64decode'] = lambda s: base64.urlsafe_b64decode(str(s).encode()).decode()
        return env

    def __init__(
            self,
            assets_path: Path | str,
            templates_path: Path | str,
            i18n: dict[str, dict[str, str]] = None,
            langs_path: Path | str = None,
            static_path: Path | str = None,
            build_langs: list[str] = None,
            html_base: str = None
    ):
        self._i18n = i18n
        self.i18n = i18n
        self.langs_path = langs_path
        self._build_langs = build_langs
        self.build_langs = build_langs

        self.template_path = Path(templates_path)
        self.assets_path = Path(assets_path)
        self.static_path = None if static_path is None else Path(static_path)
        self.html_base = '/' if html_base is None else html_base

        self.mixer = Mixer(self.assets_path)
        self.template_env = self.create_jinja_env(jinja2.FileSystemLoader(searchpath=templates_path))

    def update_langs(self):
        if self._i18n is not None:
            return
        self.i18n = self.read_langs(self.langs_path)
        self.build_langs = [lang for lang in self.i18n.keys()] if self._build_langs is None else self._build_langs

    def render(self, template: Path | str, template_vars: dict[str, str]):
        template = self.template_env.get_template(template)
        return template.render(template_vars, env=os.environ)

    def get_template_vars(self, lang: str, page: str):
        page_vars = {'base': self.html_base} | self.get_page_vars(page)
        return page_vars | recursive_substitute(page_vars, self.i18n[lang])

    def generate(self, lang: str, view: Path):
        template_vars = self.get_template_vars(lang, view.stem)
        template_name = 'views/{}'.format(view.name)
        return self.render(template_name, template_vars)

    def build_lang(self, build_path: Path | str, lang: str):
        out = Path(build_path)
        out.mkdir(parents=True, exist_ok=True)
        for view in (self.template_path / 'views').glob('*'):
            rendered = self.generate(lang, view)
            filename = out / view.name
            with open(filename, 'w') as f:
                f.write(rendered)
            print(f'{Color.OK_GREEN}Built: {Color.HEADER}{lang}{Color.RESET} {view.name:14} -> {filename}')

    def build(self, build_path: Path | str, *, version: str = None, log_release: bool = True):
        version = time.strftime('%Y-%m-%d_%H-%M-%S') if version is None else version
        build_root_path = Path(build_path)
        build_path = build_root_path / version

        print(f'{Color.INFO}Building: Version "{version}"{Color.RESET}')

        if build_path.exists():
            shutil.rmtree(build_path)

        build_path.mkdir(parents=True, exist_ok=True)

        self.update_langs()

        for lang in self.build_langs:
            self.build_lang(build_path / lang, lang)
            print()

        self.mixer.mix(build_path)

        if self.static_path is not None and self.static_path.exists():
            self.copy_static(build_path)

        if log_release:
            add_release_version(build_root_path, version)

        print(f'{Color.OK_GREEN}Build OK{os.linesep}')

    def copy_static(self, destination: Path | str):
        shutil.copytree(self.static_path, destination, dirs_exist_ok=True)


def has_symlink_privilege():
    import ctypes
    if os.name == 'nt':
        return ctypes.windll.shell32.IsUserAnAdmin() != 0
    return True


def release(build_path: Path | str, current_symlink_path: Path | str, version: str = None):
    if not has_symlink_privilege():
        print('You need to have the "Create symbolic links" privilege to release on Windows. '
              'Try running this script as an Administrator.')
        sys.exit(1)

    build_path = Path(build_path)
    current_symlink_path = Path(current_symlink_path)

    last_version = get_last_release_version(build_path) if version is None else version
    release_path = build_path / last_version
    relative_release_path = os.path.relpath(release_path, current_symlink_path.parent)

    print(f'{Color.INFO}Releasing: Version "{last_version}"{Color.RESET}')

    current_symlink_path.unlink(missing_ok=True)
    current_symlink_path.symlink_to(relative_release_path)

    print(f'{Color.OK_GREEN}Released: {current_symlink_path} -> {relative_release_path}{Color.RESET}')


def link_current_to_public(current_path: Path | str, public_path: Path | str):
    current_path = Path(current_path)
    public_path = Path(public_path)

    for target_path in current_path.glob('*/'):
        relative_path = target_path.relative_to(current_path)
        symlink_path = public_path / relative_path
        relative_target_path = os.path.relpath(target_path, symlink_path.parent)
        if symlink_path.is_symlink() or not symlink_path.exists():
            print(f'{os.linesep}{Color.INFO}Linking: {symlink_path} -> {relative_target_path}{Color.RESET}')
            try:
                symlink_path.unlink(missing_ok=True)
                symlink_path.symlink_to(relative_target_path)
                print(f'{Color.OK_GREEN}Link OK: {symlink_path} -> {relative_target_path}{Color.RESET}')
            except Exception as e:
                print(f'{Color.FAIL}Error: Symlinking failed: {symlink_path} -> {relative_target_path}{Color.RESET}')
                print(e)
        else:
            print(f'{Color.FAIL}Error: Cannot symlink: {symlink_path} -> {relative_target_path}{Color.RESET}')
