from .commands import argument_parser, execute

args = argument_parser.parse_args()
execute(args)
