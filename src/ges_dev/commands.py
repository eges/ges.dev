import argparse
import sys
import time

from pathlib import Path

from . import Color
from .watcher import watch


ROOT_PATH = Path(__file__).parent.parent.parent
DEFAULT_PUBLIC_PATH = ROOT_PATH / 'public'
DEFAULT_ASSETS_PATH = ROOT_PATH / 'public'
DEFAULT_I18N_PATH = ROOT_PATH / 'i18n'
DEFAULT_TEMPLATES_PATH = ROOT_PATH / 'templates'
DEFAULT_OUTPUT_PATH = ROOT_PATH / 'deploy' / 'releases'
DEFAULT_CURRENT_SYMLINK = ROOT_PATH / 'deploy' / 'current'


argument_parser = argparse.ArgumentParser(description='')
argp_operation = argument_parser.add_subparsers(dest='operation', help='Operation to run')

cmd_build = argp_operation.add_parser('build', help='Build static files for GES.dev')
cmd_build.add_argument('-w', '--watch', action='store_true', default=False, help='keep watching for file changes')
cmd_build.add_argument('--watch-root', metavar='PATH', default=ROOT_PATH, help='where to watch for file changes')
cmd_build.add_argument('--no-watcher-release', action='store_true', default=False, help='don\'t release watcher version')
cmd_build.add_argument('-l', '--languages', metavar='LANG', type=str, nargs='+', default=None, help='languages to compile')
cmd_build.add_argument('-t', '--templates', metavar='PATH', type=str, default=DEFAULT_TEMPLATES_PATH, help='search paths for templates')
cmd_build.add_argument('-o', '--out', metavar='PATH', type=str, default=DEFAULT_OUTPUT_PATH, help='output path for static files')
cmd_build.add_argument('-i', '--langs-path', metavar='PATH', type=str, default=DEFAULT_I18N_PATH, help='path to internationalisation files')
cmd_build.add_argument('-a', '--assets', metavar='PATH', type=str, default=DEFAULT_ASSETS_PATH, help='path to assets')
cmd_build.add_argument('-p', '--public', metavar='PATH', type=str, default=DEFAULT_PUBLIC_PATH, help='path to public docroot directory')
cmd_build.add_argument('-c', '--current', metavar='SYMLINK', type=str, default=DEFAULT_CURRENT_SYMLINK, help='path to current release symlink; only used for watcher')
cmd_build.add_argument('--base', metavar='URI', type=str, default=None, help='html base path')

cmd_release = argp_operation.add_parser('release', help='Release current build')
cmd_release.add_argument('-b', '--build', metavar='PATH', type=str, default=DEFAULT_OUTPUT_PATH, help='path to built files')
cmd_release.add_argument('-c', '--current', metavar='SYMLINK', type=str, default=DEFAULT_CURRENT_SYMLINK, help='path to current release symlink')

cmd_symlink = argp_operation.add_parser('symlink', help='Symlink from current to public')
cmd_symlink.add_argument('-c', '--current', metavar='SYMLINK', type=str, default=DEFAULT_CURRENT_SYMLINK, help='path to current release symlink')
cmd_symlink.add_argument('-p', '--public', metavar='PATH', type=str, default=DEFAULT_PUBLIC_PATH, help='path to public docroot directory')


def build(args):
    from .build import Builder, release, has_symlink_privilege
    builder = Builder(assets_path=args.assets, templates_path=args.templates, langs_path=args.langs_path,
                      build_langs=args.languages, html_base=args.base)

    if args.watch:

        watch_root = Path(args.watch_root).absolute()
        public_path = Path(args.public)
        out_path = Path(args.out)

        ignore_dirs = []

        def ignore_dir(path: Path):
            try:
                relative = watch_root / path.relative_to(watch_root)
                ignore_dirs.append(relative)
            except ValueError as e:
                print(Color.FAIL + str(e) + Color.RESET)

        ignore_dir(public_path)
        ignore_dir(out_path)

        def watch_build():
            builder.build(args.out, version='watcher', log_release=False)

        watch_build()

        if has_symlink_privilege() and not args.no_watcher_release:
            release(build_path=args.out, current_symlink_path=args.current, version='watcher')

        time.sleep(1)

        watch(watch_build, watch_root, public_path, ignore_dirs)

    else:
        builder.build(args.out)


def release(args):
    from .build import release
    release(build_path=args.build, current_symlink_path=args.current)


def symlink(args):
    from .build import link_current_to_public
    link_current_to_public(current_path=args.current, public_path=args.public)


def qqq(_):
    sys.exit(0)


def get_callback(args_or_operation):
    operation = args_or_operation if args_or_operation is str else args_or_operation.operation
    match operation:
        case 'build':
            return build
        case 'release':
            return release
        case 'symlink':
            return symlink
    return qqq


def execute(args):
    get_callback(args)(args)
