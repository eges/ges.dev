import os
import time

from pathlib import Path

from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler

from . import Color


class EventHandler(PatternMatchingEventHandler):
    def __init__(self, name, callback, patterns, ignore_dirs: list[Path] = None, **kwargs):
        super().__init__(patterns, **kwargs)
        self.name = name
        self.callback = callback
        self.ignore_dirs = [] if ignore_dirs is None else ignore_dirs

    def on_modified(self, event):
        for p in self.ignore_dirs:
            src_path = str(event.src_path)
            if src_path.startswith(str(p)) or '/.' in src_path:
                return False
        try:
            print(os.linesep + Color.INFO + 'Modified:', self.name + Color.RESET)
            self.callback()
        except Exception as e:
            print(Color.FAIL + str(e) + Color.RESET)
            return False
        return True


def watch(callback, root_path: Path | str, public_path: Path | str, ignore_dirs: list[Path]):

    # src_path = ROOT_PATH  # os.path.join(os.path.dirname(os.path.realpath(__file__)))
    src_evhandler = EventHandler('source', callback, ['*.html', '*.json', '*.xml', '*.toml'], ignore_dirs=ignore_dirs)
    src_observer = Observer()
    src_observer.schedule(src_evhandler, str(root_path), recursive=True)
    src_observer.start()

    # pub_path = './public'
    pub_evhandler = EventHandler('public', callback, ['*.js', '*.css'])
    pub_observer = Observer()
    pub_observer.schedule(pub_evhandler, str(public_path), recursive=True)
    pub_observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        src_observer.stop()
        pub_observer.stop()

    src_observer.join()
    pub_observer.join()
